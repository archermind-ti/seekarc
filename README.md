# SeekArc

### 项目介绍
滑动指示器及不同样式功能

### 效果图
![image](image/seekarc.gif)

### 安装教程

#### 方式一

1. 下载模块代码添加到自己的工程。
2. 关联使用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

    implementation project(':seekarc_library')
	……
}
```

#### 方式二

1. 在module的build.gradle中添加依赖

   ```groovy
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       ……
       implementation 'com.gitee.archermind-ti:SeekArc:1.0.1'
   }
   ```
2. 在project的build.gradle中添加`mavenCentral()`的引用

   ``` groovy
   allprojects {
       repositories {
           ……
           mavenCentral()
       }
   }
   ```

   
### 使用说明
#### 配置view 

布局xml里：
``` java
<com.triggertrap.seekarc_library.SeekArc
    ohos:id="$+id:seekArc"
    ohos:width="match_parent"
    ohos:height="match_content"
    ohos:layout_alignment="center"
    seekarc:arcColor="#ff510209"
    seekarc:clockwise="true"
    seekarc:max="150"
    ohos:margin="27vp"
    ohos:padding="3vp"
    seekarc:progressColor="#ffe2231a"
    seekarc:rotation="180"
    seekarc:startAngle="30"
    seekarc:sweepAngle="300"
    seekarc:thumb="$media:scrubber_normal"
    seekarc:touchInside="true"
    />
```
### 版本迭代

* v1.0.0
### License

```

The MIT License (MIT)

Copyright (c) 2013 Triggertrap Ltd
Author Neil Davies 

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

```