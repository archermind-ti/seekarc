/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.triggertrap.seekarc_library;

import ohos.agp.components.AttrSet;

public class Utils {
    /**
     * get the int value from AttrSet
     *
     * @param attrs the attrSet
     * @param name the attrName
     * @param defaultValue the defaultValue
     * @return int value
     */
    public static int getIntValue(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    /**
     * get the float value from AttrSet
     *
     * @param attrs the attrSet
     * @param name the attrName
     * @param defaultValue the defaultValue
     * @return float value
     */
    public static float getFloatValue(AttrSet attrs, String name, float defaultValue) {
        float value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getFloatValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    /**
     * get the boolean value from AttrSet
     *
     * @param attrs the attrSet
     * @param name the attrName
     * @param defaultValue the defaultValue
     * @return boolean value
     */
    public static boolean getBooleanValue(AttrSet attrs, String name, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getBoolValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    /**
     * get the long value from AttrSet
     *
     * @param attrs the attrSet
     * @param name the attrName
     * @param defaultValue the defaultValue
     * @return long value
     */
    public static long getLongValue(AttrSet attrs, String name, long defaultValue) {
        long value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getLongValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    /**
     * get the color value from AttrSet
     *
     * @param attrs the attrSet
     * @param name the attrName
     * @param defaultValue the defaultValue
     * @return int colorValue
     */
    public static int getColorValue(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }
}
