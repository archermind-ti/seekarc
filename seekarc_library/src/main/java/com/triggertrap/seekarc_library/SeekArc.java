package com.triggertrap.seekarc_library;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentState;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.icu.util.Measure;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;

public class SeekArc extends Component implements Component.EstimateSizeListener, Component.ComponentStateChangedListener {
    private static final String TAG = SeekArc.class.getSimpleName();
    private static int INVALID_PROGRESS_VALUE = -1;
    public static boolean scrollEnabled = true;
    // The initial rotational offset -90 means we start at 12 o'clock
    private final int mAngleOffset = -90;

    /**
     * The Drawable for the seek arc thumbnail
     */
    private PixelMapElement mThumb;

    /**
     * The Maximum value that this SeekArc can be set to
     */
    private int mMax = 100;

    /**
     * The Current value that the SeekArc is set to
     */
    private int mProgress = 0;

    /**
     * The width of the progress line for this SeekArc
     */
    private int mProgressWidth = 4;

    /**
     * The Width of the background arc for the SeekArc
     */
    private int mArcWidth = 2;

    /**
     * The Angle to start drawing this Arc from
     */
    private int mStartAngle = 0;

    /**
     * The Angle through which to draw the arc (Max is 360)
     */
    private int mSweepAngle = 360;

    /**
     * The rotation of the SeekArc- 0 is twelve o'clock
     */
    private int mRotation = 0;

    /**
     * Give the SeekArc rounded edges
     */
    private boolean mRoundedEdges = false;

    /**
     * Enable touch inside the SeekArc
     */
    private boolean mTouchInside = true;

    /**
     * Will the progress increase clockwise or anti-clockwise
     */
    private boolean mClockwise = true;


    /**
     * is the control enabled/touchable
     */
    private boolean mEnabled = true;

    // Internal variables
    private int mArcRadius = 0;
    private float mProgressSweep = 0;
    private RectFloat mArcRect = new RectFloat();
    private Paint mArcPaint;
    private Paint mProgressPaint;
    private int mTranslateX;
    private int mTranslateY;
    private int mThumbXPos;
    private int mThumbYPos;
    private double mTouchAngle;
    private float mTouchIgnoreRadius;
    private OnSeekArcChangeListener mOnSeekArcChangeListener;

    public SeekArc(Context context) {
        this(context, null);
    }

    public SeekArc(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public SeekArc(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }

    @Override
    public void onComponentStateChanged(Component component, int i) {
        int thumbHalfheight = 0;
        int thumbHalfWidth = 0;
    }


    public interface OnSeekArcChangeListener {
        void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser);

        void onStartTrackingTouch(SeekArc seekArc);

        void onStopTrackingTouch(SeekArc seekArc);
    }

    private void init(Context context, AttrSet attrs) {

        setComponentStateChangedListener(getComponentStateChangedListener());
        setEstimateSizeListener(this);

        final ResourceManager res = getResourceManager();
        float density = DisplayManager.getInstance().getDefaultDisplay(this.getContext()).get().getAttributes().densityPixels;

        // Defaults, may need to link this into theme settings
        int arcColor = 0;
        int progressColor = 0;
        try {
            arcColor = res.getElement(ResourceTable.Color_progress_gray).getColor();
            progressColor = res.getElement(ResourceTable.Color_default_blue_light).getColor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        int thumbHalfheight = 0;
        int thumbHalfWidth = 0;
        mThumb = getPixMapRes(ResourceTable.Media_scrubber_control_normal_holo);

//         Convert progress width to pixels for current density
        mProgressWidth = (int) (mProgressWidth * density);


        thumbHalfheight = mThumb.getHeight() / 2;
        thumbHalfWidth = mThumb.getWidth() / 2;

        thumbHalfheight = 100 / 2;
        thumbHalfWidth = 100 / 2;

        mMax = Utils.getIntValue(attrs, "max", mMax);
        mProgress = Utils.getIntValue(attrs, "progress", mProgress);
        mProgressWidth = Utils.getIntValue(attrs, "progressWidth", mProgressWidth);
        mArcWidth = Utils.getIntValue(attrs, "mArcWidth", mArcWidth);
        mStartAngle = Utils.getIntValue(attrs, "startAngle", mStartAngle);
        mSweepAngle = Utils.getIntValue(attrs, "sweepAngle", mSweepAngle);
        mRotation = Utils.getIntValue(attrs, "rotation", mRotation);
        mRoundedEdges = Utils.getBooleanValue(attrs, "roundEdges", mRoundedEdges);
        mTouchInside = Utils.getBooleanValue(attrs, "touchInside", mTouchInside);
        mClockwise = Utils.getBooleanValue(attrs, "clockwise", mClockwise);
        mEnabled = Utils.getBooleanValue(attrs, "enabled", mEnabled);

        arcColor = Utils.getColorValue(attrs, "arcColor", arcColor);
        progressColor = Utils.getColorValue(attrs, "progressColor", progressColor);


        mProgress = Math.min(mProgress, mMax);
        mProgress = Math.max(mProgress, 0);

        mSweepAngle = Math.min(mSweepAngle, 360);
        mSweepAngle = Math.max(mSweepAngle, 0);

        mProgressSweep = (float) mProgress / mMax * mSweepAngle;

        mStartAngle = (mStartAngle > 360) ? 0 : mStartAngle;
        mStartAngle = Math.max(mStartAngle, 0);

        mArcPaint = new Paint();
        mArcPaint.setColor(new Color(arcColor));
        mArcPaint.setAntiAlias(true);
        mArcPaint.setStyle(Paint.Style.STROKE_STYLE);
        mArcPaint.setStrokeWidth(mArcWidth);
        //mArcPaint.setAlpha(45);

        mProgressPaint = new Paint();
        mProgressPaint.setColor(new Color(progressColor));
        mProgressPaint.setAntiAlias(true);
        mProgressPaint.setStyle(Paint.Style.STROKE_STYLE);
        mProgressPaint.setStrokeWidth(mProgressWidth);

        if (mRoundedEdges) {
            mArcPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
            mProgressPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        }
        addDrawTask(new DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                if (!mClockwise) {
                    canvas.scale(-1, 1, mArcRect.getHorizontalCenter(),
                            mArcRect.getVerticalCenter());

                }

                // Draw the arcs
                final int arcStart = mStartAngle + mAngleOffset + mRotation;
                final int arcSweep = mSweepAngle;
                Arc arc = new Arc();
                Arc arc2 = new Arc();

                arc.setArc(arcStart, arcSweep, false);
                arc2.setArc(arcStart, mProgressSweep, false);
                canvas.drawArc(mArcRect, arc, mArcPaint);
                canvas.drawArc(mArcRect, arc2, mProgressPaint);

//                =======
                if (mEnabled) {
                    // Draw the thumb nail
                    System.out.println("======mTranslateX - mThumbXPos: "+(mTranslateX - mThumbXPos));
//                    System.out.println("mTranslateY - mThumbYPos: "+(mTranslateY - mThumbYPos));
                    canvas.translate(mTranslateX - mThumbXPos-30, mTranslateY - mThumbYPos-20);

                    mThumb.drawToCanvas(canvas);
                }
            }
        });


        setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                float Y = touchEvent.getPointerPosition(0).getY();
                if (mEnabled) {

                    switch (touchEvent.getAction()) {
                        case TouchEvent.PRIMARY_POINT_DOWN:
                            if(Y >getContentPositionY() &&Y<getContentPositionY()+getHeight()){
                                scrollEnabled = false;
                            }
                            onStartTrackingTouch();
                            updateOnTouch(touchEvent);
                            break;
                        case TouchEvent.POINT_MOVE:
                            updateOnTouch(touchEvent);
                            break;
                        case TouchEvent.PRIMARY_POINT_UP:
                            scrollEnabled = true;
                            onStopTrackingTouch();
                            setPressState(false);
                            break;
                        case TouchEvent.CANCEL:
                            onStopTrackingTouch();
                            setPressState(false);
                            break;
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public boolean isEnabledScroll(){
        return scrollEnabled;
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int width = EstimateSpec.getSize(widthMeasureSpec);
        int height = EstimateSpec.getSize(heightMeasureSpec);

        final int min = Math.min(width, height);
        float top = 0;
        float left = 0;
        int arcDiameter = 0;

        mTranslateX = (int) (width * 0.5f);
        mTranslateY = (int) (height * 0.5f);

        arcDiameter = min - getPaddingLeft()-80;
        mArcRadius = (arcDiameter) / 2;
        top = height / 2f - (arcDiameter / 2f);
        left = width / 2f - (arcDiameter / 2f);
        mArcRect = new RectFloat(left, top+10, left + arcDiameter, top + arcDiameter+10);

        int arcStart = (int) mProgressSweep + mStartAngle + mRotation + 90;
        mThumbXPos = (int) (mArcRadius * Math.cos(Math.toRadians(arcStart)));
        mThumbYPos = (int) (mArcRadius * Math.sin(Math.toRadians(arcStart)));
        return false;
    }

    private void onStartTrackingTouch() {
        if (mOnSeekArcChangeListener != null) {
            mOnSeekArcChangeListener.onStartTrackingTouch(this);
        }

    }

    private void updateOnTouch(TouchEvent event) {
        boolean ignoreTouch = ignoreTouch(event.getPointerPosition(0).getX(), event.getPointerPosition(0).getY());
        if (ignoreTouch) {
            return;
        }
        setPressState(true);
        mTouchAngle = getTouchDegrees(event.getPointerPosition(0).getX(), event.getPointerPosition(0).getY());
        int progress = getProgressForAngle(mTouchAngle);
        onProgressRefresh(progress, true);
    }

    private boolean ignoreTouch(float xPos, float yPos) {
        boolean ignore = false;
        float x = xPos - mTranslateX;
        float y = yPos - mTranslateY;

        float touchRadius = (float) Math.sqrt(((x * x) + (y * y)));
        if (touchRadius < mTouchIgnoreRadius) {
            ignore = true;
        }
        return ignore;
    }

    private double getTouchDegrees(float xPos, float yPos) {
        float x = xPos - mTranslateX;
        float y = yPos - mTranslateY;
        //invert the x-coord if we are rotating anti-clockwise
        x = (mClockwise) ? x : -x;
        // convert to arc Angle
        double angle = Math.toDegrees(Math.atan2(y, x) + (Math.PI / 2)
                - Math.toRadians(mRotation));
        if (angle < 0) {
            angle = 360 + angle;
        }
        angle -= mStartAngle;
        return angle;
    }

    private int getProgressForAngle(double angle) {
        int touchProgress = (int) Math.round(valuePerDegree() * angle);

        touchProgress = (touchProgress < 0) ? INVALID_PROGRESS_VALUE
                : touchProgress;
        touchProgress = (touchProgress > mMax) ? INVALID_PROGRESS_VALUE
                : touchProgress;
        return touchProgress;
    }

    private float valuePerDegree() {
        return (float) mMax / mSweepAngle;
    }

    private void onProgressRefresh(int progress, boolean fromUser) {
        updateProgress(progress, fromUser);
    }


    private void updateProgress(int progress, boolean fromUser) {

        if (progress == INVALID_PROGRESS_VALUE) {
            return;
        }

        progress = Math.min(progress, mMax);
        progress = Math.max(progress, 0);
        mProgress = progress;

        if (mOnSeekArcChangeListener != null) {
            mOnSeekArcChangeListener
                    .onProgressChanged(this, progress, fromUser);
        }

        mProgressSweep = (float) progress / mMax * mSweepAngle;

        updateThumbPosition();

        invalidate();
    }

    private void updateThumbPosition() {
        int thumbAngle = (int) (mStartAngle + mProgressSweep + mRotation + 90);
        mThumbXPos = (int) (mArcRadius * Math.cos(Math.toRadians(thumbAngle)));
        mThumbYPos = (int) (mArcRadius * Math.sin(Math.toRadians(thumbAngle)));
    }

    private void onStopTrackingTouch() {
        if (mOnSeekArcChangeListener != null) {
            mOnSeekArcChangeListener.onStopTrackingTouch(this);
        }
    }

    public void setOnSeekArcChangeListener(OnSeekArcChangeListener l) {
        mOnSeekArcChangeListener = l;
    }

    public void setProgress(int progress) {
        updateProgress(progress, false);
    }

    public int getProgress() {
        return mProgress;
    }

    public int getProgressWidth() {
        return mProgressWidth;
    }

    public void setProgressWidth(int mProgressWidth) {
        this.mProgressWidth = mProgressWidth;
        mProgressPaint.setStrokeWidth(mProgressWidth);
    }

    public int getArcWidth() {
        return mArcWidth;
    }

    public void setArcWidth(int mArcWidth) {
        this.mArcWidth = mArcWidth;
        mArcPaint.setStrokeWidth(mArcWidth);
    }

    public int getArcRotation() {
        return mRotation;
    }

    public void setArcRotation(int mRotation) {
        this.mRotation = mRotation;
        updateThumbPosition();
    }

    public int getStartAngle() {
        return mStartAngle;
    }

    public void setStartAngle(int mStartAngle) {
        this.mStartAngle = mStartAngle;
        updateThumbPosition();
    }

    public int getSweepAngle() {
        return mSweepAngle;
    }

    public void setSweepAngle(int mSweepAngle) {
        this.mSweepAngle = mSweepAngle;
        updateThumbPosition();
    }

    public void setRoundedEdges(boolean isEnabled) {
        mRoundedEdges = isEnabled;
        if (mRoundedEdges) {
            mArcPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
            mProgressPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        } else {
            mArcPaint.setStrokeCap(Paint.StrokeCap.SQUARE_CAP);
            mProgressPaint.setStrokeCap(Paint.StrokeCap.SQUARE_CAP);
        }
    }

    public void setTouchInSide(boolean isEnabled) {
        int thumbHalfheight = (int) mThumb.getHeight() / 2;
        int thumbHalfWidth = (int) mThumb.getWidth() / 2;
        mTouchInside = isEnabled;
        if (mTouchInside) {
            mTouchIgnoreRadius = (float) mArcRadius / 4;
        } else {
            // Don't use the exact radius makes interaction too tricky
            mTouchIgnoreRadius = mArcRadius
                    - Math.min(thumbHalfWidth, thumbHalfheight);
        }
    }

    public void setmThumb(int i){
        if(i==1){
            mThumb= getPixMapRes(ResourceTable.Media_scrubber_control_normal_holo);
        }else {
            mThumb= getPixMapRes(ResourceTable.Media_scrubber_normal);
        }
    }

    public void setClockwise(boolean isClockwise) {
        mClockwise = isClockwise;
    }

    public boolean isClockwise() {
        return mClockwise;
    }

    public boolean isEnabled() {
        return mEnabled;
    }

    public void setEnabled(boolean enabled) {
        this.mEnabled = enabled;
    }

    public Color getProgressColor() {
        return mProgressPaint.getColor();
    }

    public void setProgressColor(int color) {
        mProgressPaint.setColor(new Color(color));

        invalidate();
    }

    public Color getArcColor() {
        return mArcPaint.getColor();
    }

    public void setArcColor(int color) {
        mArcPaint.setColor(new Color(color));
        invalidate();
    }

    public int getMax() {
        return mMax;
    }

    public void setMax(int mMax) {
        this.mMax = mMax;
    }

    private PixelMapElement getPixMapRes(int resourceId){
        Resource resource = null;
        try {
            resource = getResourceManager().getResource(resourceId);
            ImageSource imageSource = ImageSource.create(resource, null);
            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
            PixelMap pixelmap = imageSource.createPixelmap(decodingOpts);

            return new PixelMapElement(pixelmap);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        return null;
    }
}
