package com.triggertrap.seekarc;

import com.triggertrap.seekarc.slice.CustomActivitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CustomActivity extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CustomActivitySlice.class.getName());
    }
}
