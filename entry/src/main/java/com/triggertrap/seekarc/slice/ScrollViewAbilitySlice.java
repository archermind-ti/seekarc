package com.triggertrap.seekarc.slice;

import com.triggertrap.seekarc.ResourceTable;
import com.triggertrap.seekarc_library.SeekArc;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DragInfo;
import ohos.agp.components.ScrollView;
import ohos.multimodalinput.event.TouchEvent;

public class ScrollViewAbilitySlice extends AbilitySlice implements Component.TouchEventListener {
    ScrollView scrollView ;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_activity_scrollview);
        scrollView = (ScrollView) findComponentById(ResourceTable.Id_scrollView);
        scrollView.setTouchEventListener(this);

    }



    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        SeekArc seekArc = (SeekArc) findComponentById(ResourceTable.Id_seekArc);
        float Y = touchEvent.getPointerPosition(0).getY();
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            scrollView.setEnabled(!(Y > seekArc.getContentPositionY()) || !(Y < (seekArc.getHeight() + seekArc.getContentPositionY())));
        }
        return true;
    }
}
