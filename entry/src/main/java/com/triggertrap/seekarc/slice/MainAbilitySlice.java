package com.triggertrap.seekarc.slice;

import com.triggertrap.seekarc.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        DirectionalLayout SeekArc = (DirectionalLayout) findComponentById(ResourceTable.Id_SeekArc);
        DirectionalLayout CustomStyle = (DirectionalLayout) findComponentById(ResourceTable.Id_CustomStyle);
        DirectionalLayout scrollView = (DirectionalLayout) findComponentById(ResourceTable.Id_scrollview);
        DirectionalLayout Disabled = (DirectionalLayout) findComponentById(ResourceTable.Id_Disabled);

//        SeekArc.setClickedListener(new Component.ClickedListener() {
//            @Override
//            public void onClick(Component component) {
//                present(new SimpleActivitySlice(),new Intent());
//            }
//        });
//
//        CustomStyle.setClickedListener(new Component.ClickedListener() {
//            @Override
//            public void onClick(Component component) {
//                present(new CustomActivitySlice(),new Intent());
//            }
//        });
//
//        scrollView.setClickedListener(new Component.ClickedListener() {
//            @Override
//            public void onClick(Component component) {
//                present(new ScrollViewAbilitySlice(),new Intent());
//            }
//        });
//
//        Disabled.setClickedListener(new Component.ClickedListener() {
//            @Override
//            public void onClick(Component component) {
//                present(new DisabledActivitySlice(),new Intent());
//            }
//        });
//    }

        SeekArc.setClickedListener(component -> {
            Intent intent1 = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName("SimpleActivity")
                    .build();
            intent1.setOperation(operation);
            startAbility(intent1);
        });

        CustomStyle.setClickedListener(component -> {
            Intent intent1 = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName("CustomActivity")
                    .build();
            intent1.setOperation(operation);
            startAbility(intent1);
        });

        scrollView.setClickedListener(component -> {
            Intent intent1 = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName("ScrollViewAbility")
                    .build();
            intent1.setOperation(operation);
            startAbility(intent1);
        });

        Disabled.setClickedListener(component -> {
            Intent intent1 = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName("DisabledActivity")
                    .build();
            intent1.setOperation(operation);
            startAbility(intent1);
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
