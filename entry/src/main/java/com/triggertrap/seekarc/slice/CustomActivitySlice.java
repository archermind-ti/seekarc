package com.triggertrap.seekarc.slice;

import com.triggertrap.seekarc.ResourceTable;
import com.triggertrap.seekarc_library.SeekArc;
import ohos.aafwk.content.Intent;

public class CustomActivitySlice extends SimpleActivitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        SeekArc seekArc = (SeekArc) findComponentById(ResourceTable.Id_seekArc);
        seekArc.setmThumb(2);
    }

    @Override
    protected int getLayoutFile() {
        return ResourceTable.Layout_custom_sample;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
