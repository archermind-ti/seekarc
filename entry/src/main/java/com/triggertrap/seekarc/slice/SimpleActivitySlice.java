package com.triggertrap.seekarc.slice;

import com.triggertrap.seekarc.ResourceTable;
import com.triggertrap.seekarc_library.SeekArc;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AbsButton;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;

public class SimpleActivitySlice extends AbilitySlice {
    private SeekArc mSeekArc;
    private Slider mRotation;
    private Slider mStartAngle;
    private Slider mSweepAngle;
    private Slider mArcWidth;
    private Slider mProgressWidth;
    private Checkbox mRoundedEdges;
    private Checkbox mTouchInside;
    private Checkbox mClockwise;
    private Text mSeekArcProgress;
    private Checkbox mEnabled;

    protected int getLayoutFile(){
        return ResourceTable.Layout_holo_sample;
    }
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(getLayoutFile());

        mSeekArc = (SeekArc) findComponentById(ResourceTable.Id_seekArc);
        mSeekArcProgress = (Text) findComponentById(ResourceTable.Id_seekArcProgress);
        mRotation = (Slider) findComponentById(ResourceTable.Id_rotation);
        mStartAngle = (Slider) findComponentById(ResourceTable.Id_startAngle);
        mSweepAngle  = (Slider) findComponentById(ResourceTable.Id_sweepAngle);
        mArcWidth = (Slider) findComponentById(ResourceTable.Id_arcWidth);
        mProgressWidth = (Slider) findComponentById(ResourceTable.Id_progressWidth);
        mRoundedEdges = (Checkbox) findComponentById(ResourceTable.Id_roundedEdges);
        mTouchInside = (Checkbox) findComponentById(ResourceTable.Id_touchInside);
        mClockwise = (Checkbox) findComponentById(ResourceTable.Id_clockwise);
        mEnabled = (Checkbox) findComponentById(ResourceTable.Id_enabled);

        mRotation.setProgressValue(mSeekArc.getArcRotation());
        mStartAngle.setProgressValue(mSeekArc.getStartAngle());
        mSweepAngle.setProgressValue(mSeekArc.getSweepAngle());
        mArcWidth.setProgressValue(mSeekArc.getArcWidth());
        mProgressWidth.setProgressValue(mSeekArc.getProgressWidth());

        mSeekArc.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
            @Override
            public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
                mSeekArcProgress.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekArc seekArc) {

            }

            @Override
            public void onStopTrackingTouch(SeekArc seekArc) {

            }
        });

        mRotation.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                mSeekArc.setArcRotation(i);
                mSeekArc.invalidate();
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mStartAngle.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                mSeekArc.setStartAngle(i);
                mSeekArc.invalidate();
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mSweepAngle.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                mSeekArc.setSweepAngle(i);
                mSeekArc.invalidate();
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mArcWidth.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                mSeekArc.setArcWidth(i);
                mSeekArc.invalidate();
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mProgressWidth.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                mSeekArc.setProgressWidth(i);
                mSeekArc.invalidate();
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mRoundedEdges.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                mSeekArc.setRoundedEdges(b);
                mSeekArc.invalidate();
            }
        });

        mTouchInside.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                mSeekArc.setTouchInSide(b);
            }
        });

        mClockwise.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                mSeekArc.setClockwise(b);
                mSeekArc.invalidate();
            }
        });

        mEnabled.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                mSeekArc.setEnabled(b);
                mSeekArc.invalidate();
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
