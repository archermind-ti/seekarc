package com.triggertrap.seekarc.slice;

import com.triggertrap.seekarc.ResourceTable;
import com.triggertrap.seekarc_library.SeekArc;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.media.image.Image;
import ohos.media.image.ImageSource;

public class DisabledActivitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_activity_disabled);

        SeekArc seekArcComplete = (SeekArc) findComponentById(ResourceTable.Id_seekArcComplete);
        SeekArc seekArcWarning = (SeekArc) findComponentById(ResourceTable.Id_seekArcWarning);
        seekArcComplete.setArcWidth(20);
        seekArcComplete.setProgressWidth(20);
        seekArcComplete.setProgressColor(0xff00ff00);
        seekArcWarning.setArcWidth(20);
        seekArcWarning.setProgressWidth(20);
        seekArcWarning.setProgressColor(0xffff0000);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
