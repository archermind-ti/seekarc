package com.triggertrap.seekarc;

import com.triggertrap.seekarc.slice.ScrollViewAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ScrollViewAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ScrollViewAbilitySlice.class.getName());
    }
}
