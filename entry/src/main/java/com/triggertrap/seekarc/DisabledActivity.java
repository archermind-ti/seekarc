package com.triggertrap.seekarc;

import com.triggertrap.seekarc.slice.DisabledActivitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class DisabledActivity extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DisabledActivitySlice.class.getName());
    }
}
