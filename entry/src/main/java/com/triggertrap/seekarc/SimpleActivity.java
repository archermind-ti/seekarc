package com.triggertrap.seekarc;

import com.triggertrap.seekarc.slice.SimpleActivitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SimpleActivity extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SimpleActivitySlice.class.getName());
    }

}
